module Networking.DirectMessaging where

import Util.DirectMessage
import qualified Data.Text as T
import Networking.Response
import Database.Types
import Util.User
import Data.Time
import Control.Monad (forM)
import Data.Maybe (catMaybes)
import Networking.Statuscodes

handleSendDirectMessage :: User -> T.Text -> T.Text -> IO Response
handleSendDirectMessage sender tRecipient message = do
    mRecipient <- getUserByName tRecipient
    case mRecipient of
        Nothing -> return $ emptyResponse RecipientDoesNotExist
        Just recipient -> do
            sendDirectMessage $ DirectMessageWrite (userid sender) (userid recipient) message
            return okResponse

handleGetDirectMessages :: User -> UTCTime -> IO Response
handleGetDirectMessages user time = do
    dms <- getDirectMessagesForUserSinceTime user time
    rDms <- forM dms messageLikeToResponseMessage
    return $ Response 0.3 Ok $ DMResponseBody (catMaybes rDms)

handleGetAllDirectMessages :: User -> IO Response
handleGetAllDirectMessages user = do
    dms <- getDirectMessagesForUser user
    rDms <- forM dms messageLikeToResponseMessage
    return $ Response 0.3 Ok $ DMResponseBody (catMaybes rDms)
