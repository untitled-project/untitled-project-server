FROM nixos/nix

WORKDIR /server

RUN nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
RUN nix-channel --update
RUN nix-env --install stack

COPY . .

RUN sed -i 's/nix:/nix:\n    enable: true/' stack.yaml
RUN stack setup
RUN stack install

CMD /root/.local/bin/untitled-project-server

