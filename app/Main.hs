module Main where

import Networking.Server

main :: IO ()
main = mkServer "2599" "2598" >>= run
