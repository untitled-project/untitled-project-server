{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Util.DirectMessage where

import Util.User
import Database.Database
import Database.Types
import Database.PostgreSQL.Simple
import Data.Time
import qualified Data.Text as T

-- |Adds a direct message to the Database. Returns the message on success, Nothing on failure.
sendDirectMessage :: DirectMessageWrite -> IO DirectMessage
sendDirectMessage dm = head <$> (withDatabase $ \conn -> query conn "insert into direct_message (source, destination, directMessage) values (?, ?, ?) returning *" dm)

-- |Returns all Messages which are sent to a user. the userid of the user must be set.
getDirectMessagesForUser :: User -> IO [DirectMessage]
getDirectMessagesForUser user = withDatabase $ \conn -> query conn "select * from direct_message where destination = ?" $ Only (userid user)

getDirectMessagesForUserSinceTime :: User -> UTCTime -> IO [DirectMessage]
getDirectMessagesForUserSinceTime user time = withDatabase $ \conn -> query conn "select * from direct_message where destination = ? and directmessagesent < ?" (userid user, time)
