{-# LANGUAGE OverloadedStrings #-}
module Database.Database (withDatabase) where

import Database.PostgreSQL.Simple
import Control.Exception (bracket)

connectionString = "host=localhost dbname=postgres user=postgres password=password"

-- |Executes a function on the database and returns the result.
withDatabase :: (Connection -> IO a) -> IO a
withDatabase = bracket (connectPostgreSQL connectionString) close

