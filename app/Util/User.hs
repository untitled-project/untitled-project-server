{-# LANGUAGE OverloadedStrings #-}
module Util.User where

import Database.Database
import Database.Types
import Util.Password
import Database.PostgreSQL.Simple
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE (encodeUtf8)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Control.Monad (void)
import Data.Maybe (listToMaybe, isJust)

{-|Adds a user to the database.
   Returns the updated user on success.
   If the user already exists, Nothing will be returned.
-}
addUser :: T.Text -> T.Text  -> IO (Maybe User)
addUser username password = do
        alreadyExists <- doesUserExist username
        (hashedPassword, salt) <- createSaltAndHash password
        if alreadyExists then
            return Nothing
        else
            listToMaybe <$> (withDatabase $ \conn -> query conn "insert into public.user (username, password, salt) values (?, ?, ?) returning *" (UserWrite username hashedPassword salt))
            
-- |Removes a user from the database.
deleteUser :: T.Text -> IO ()
deleteUser username = void . withDatabase $ \conn -> execute conn "delete from public.user where username = ?" $ Only username

-- |Returns a User if the username exists, else Nothing.
getUserByName :: T.Text -> IO (Maybe User)
getUserByName name = listToMaybe <$> (withDatabase $ \conn -> query conn "select * from public.user where username=?" $ Only name)

-- |Returns a User if the userid exists, else Nothing
getUserById :: Int -> IO (Maybe User)
getUserById id = listToMaybe <$> (withDatabase $ \conn -> query conn "select * from public.user where userid=?" $ Only id)

-- |Checks if a user with the provided username exists in the database
doesUserExist :: T.Text -> IO Bool
doesUserExist name = isJust <$> getUserByName name

-- |Checks if username and password are correct. Returns the user on success, Nothing on failure.
authenticate :: T.Text -> T.Text -> IO (Maybe User)
authenticate providedUsername providedPassword = do
        authenticate' <$> getUserByName providedUsername
            where
                authenticate' :: Maybe User -> Maybe User 
                authenticate' mUser = case mUser of
                    Nothing -> Nothing
                    Just user -> if hashWithSalt (BSL.fromStrict . TE.encodeUtf8 $ providedPassword) (BSL.fromStrict $ salt user) == (BSL.fromStrict $ password user) then
                            Just user
                        else 
                            Nothing

{-|Changes the password of the specified user if the provided current password
   is correct. Returns Just User on success, Nothing of failure.
-}
changePassword :: T.Text -> T.Text -> T.Text -> IO (Maybe User)
changePassword username old new = do
        mUser <- authenticate username old
        case mUser of
            Nothing -> return Nothing
            Just user -> do
                (hashedNew, newSalt) <- createSaltAndHash new
                listToMaybe <$>
                    ( withDatabase $ \conn -> query conn "update public.user set password = ?, salt = ? where userid = ? returning userid, username, password, salt" 
                        ( hashedNew
                        , newSalt
                        , userid user)
                    )

-- |Returns a list of all registered users
getAllUsers :: IO [User]
getAllUsers = withDatabase $ \conn -> query_ conn "select * from public.user"
