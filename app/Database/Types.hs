{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Database.Types where

import Data.Text
import Data.ByteString
import GHC.Generics (Generic)
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.ToField
import Data.Text.Encoding (encodeUtf8)
import Data.Time

class MessageLike a where
    message :: a -> Text
    senderId :: a -> Int
    messageSent :: a -> UTCTime

data User = User
        { userid :: Int
        , username :: Text
        , password :: ByteString
        , salt :: ByteString
        } deriving (Generic, FromRow, Show)

data UserWrite = UserWrite
    { usernameWrite :: Text
    , passwordWrite :: ByteString
    , saltWrite :: ByteString
    } deriving (Show)

instance ToRow UserWrite where
    toRow user = [(toField $ usernameWrite user), (EscapeByteA $ passwordWrite user), (EscapeByteA $ saltWrite user)]

data DirectMessage = DirectMessage
        { directmessageid :: Int
        , directmessagesent :: UTCTime 
        , source :: Int
        , destination :: Int
        , directmessage :: Text
        } deriving (Generic, FromRow, Show)

instance MessageLike DirectMessage where
    message = directmessage
    senderId = source
    messageSent = directmessagesent

data DirectMessageWrite = DirectMessageWrite
        { sourceWrite :: Int
        , destinationWrite :: Int
        , directmessageWrite :: Text
        } deriving (Show)

instance ToRow DirectMessageWrite where
    toRow dm = [(toField $ sourceWrite dm), (toField $ destinationWrite dm), (toField $ directmessageWrite dm)]

data Groupchat = Groupchat
        { groupchatid :: Int
        , groupchatcreated :: UTCTime
        , groupchatname :: Text
        } deriving (Generic, FromRow, Show)

data GroupchatWrite = GroupchatWrite
        { groupchatnameWrite :: Text
        } deriving (Show)

instance ToRow GroupchatWrite where
    toRow gc = [(toField $ groupchatnameWrite gc)]

data GroupMember = GroupMember
        { groupmemberid :: Int
        , groupmembersince :: UTCTime
        , groupmember :: Int
        , groupmemberof :: Int
        } deriving (Generic, FromRow, Show)

data GroupMemberWrite = GroupMemberWrite
        { groupmemberWrite :: Int
        , groupmemberofWrite :: Int
        } deriving (Show)

instance ToRow GroupMemberWrite where
    toRow gm = [(toField $ groupmemberWrite gm)] -- ???

data GroupAdmin = GroupAdmin
        { groupadminid :: Int
        , groupadminsince :: UTCTime
        , groupadmin :: Int
        , groupadminof :: Int
        } deriving (Generic, FromRow, Show)

data GroupAdminWrite = GroupAdminWrite
        { groupadminWrite :: Int
        , groupadminofWrite :: Int
        } deriving (Show)

instance ToRow GroupAdminWrite where
        toRow ga = [(toField $ groupadminWrite ga),(toField $ groupadminofWrite ga)]
 
data GroupMessage = GroupMessage
        { groupmessageid :: Int
        , groupmessagesent :: UTCTime
        , groupmessagesource :: Int
        , groupmessagedestination :: Int
        , groupmessage :: Text
        } deriving (Generic, FromRow, Show)

instance MessageLike GroupMessage where
    message = groupmessage
    senderId = groupmessagesource
    messageSent = groupmessagesent

data GroupMessageWrite = GroupMessageWrite
        { groupmessagesourceWrite :: Int
        , groupmessagedestinationWrite :: Int
        , groupmessageWrite :: Text
        } deriving (Show)

instance ToRow GroupMessageWrite where
    toRow gm = [(toField $ groupmessagesourceWrite gm), (toField $ groupmessagedestinationWrite gm), (toField $ groupmessageWrite gm)]

