module Networking.GroupMessaging where

import Networking.Response
import Database.Database
import Database.Types
import Util.GroupChat
import qualified Data.Text as T
import Control.Monad (forM)
import Data.Maybe (catMaybes)
import Networking.Statuscodes
import Util.User (getUserByName)

handleCreateGroup :: T.Text -> IO Response
handleCreateGroup groupName = do
    mGroupchat <- addGroupchat groupName
    case mGroupchat of
        Nothing -> return $ emptyResponse GroupNameAlreadyTaken
        _ -> return okResponse        

handleJoinGroup :: User -> T.Text -> T.Text -> IO Response 
handleJoinGroup admin newMemberName groupName = do
    mGroup <- getGroupchatByName groupName
    case mGroup of
        Nothing -> return $ emptyResponse GroupDoesNotExist
        Just group -> do
            isAdmin <- isGroupAdmin admin group
            if not isAdmin then do
                return $ emptyResponse NotAnAdmin
            else do
                mNewMember <- getUserByName newMemberName
                case mNewMember of
                    Nothing -> return $ emptyResponse UserDoesNotExist
                    Just newMember -> do
                        inChat <- isUserInGroupchat newMember group
                        if inChat then
                            return okResponse
                        else do
                            success <- addUserToGroupchat newMember group
                            case success of 
                                Nothing -> return $ emptyResponse InternalServerError
                                Just gm -> return okResponse 

handleGetAllGroupMessages :: User -> T.Text -> IO Response
handleGetAllGroupMessages user groupName = do
    mGroup <- getGroupchatByName groupName
    case mGroup of
        Nothing -> return $ emptyResponse GroupDoesNotExist
        Just group -> do
            inChat <- isUserInGroupchat user group
            if not inChat then
                return $ emptyResponse UserIsNotInGroup
            else do
                mTime <- sinceWhenIsUserInGroupchat user group
                case mTime of
                    Nothing -> return $ emptyResponse InternalServerError -- already checked if user is in group, should not happen
                    Just time -> do
                        gms <- getGroupMessagesSince time group
                        rGms <- forM gms messageLikeToResponseMessage
                        return $ Response 0.3 Ok $ GroupMessagesResponseBody (groupchatname group) (catMaybes rGms)

handleAddGroupAdmin :: User -> T.Text -> T.Text -> IO Response
handleAddGroupAdmin admin newAdminName groupName = undefined
