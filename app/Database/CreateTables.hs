{-# LANGUAGE OverloadedStrings #-}
module Database.CreateTables (createTables) where

import Database.PostgreSQL.Simple
import Control.Monad (void)
import Database.Database

-- |Creates all neccessary tables in the PostgreSQL Database
createTables :: IO ()
createTables = createUserTable 
            >> createDirectMesageTable
            >> createGroupchatTable
            >> createGroupMemberTable
            >> createGroupMessageTable
            >> createGroupAdminTable

createUserTable :: IO ()
createUserTable = void . withDatabase $
                \conn -> execute_ conn "create table if not exists public.user (userid serial primary key, username varchar(255) not null, password bytea not null, salt bytea not null)"

createDirectMesageTable :: IO ()
createDirectMesageTable = void . withDatabase $
        \conn -> execute_ conn "create table if not exists direct_message (directmessageid serial primary key, directmessagesent timestamptz default CURRENT_TIMESTAMP, source integer references public.user(userid) not null, destination integer references public.user(userid) not null, directmessage varchar(1024) not null)"

createGroupchatTable :: IO ()
createGroupchatTable = void . withDatabase $
        \conn -> execute_ conn "create table if not exists group_chat (groupchatid serial primary key, groupchatcreated timestamptz default CURRENT_TIMESTAMP, groupchatname varchar(255) not null)"

createGroupMemberTable :: IO ()
createGroupMemberTable = void . withDatabase $
        \conn -> execute_ conn "create table if not exists group_member (groupmemberid serial primary key, groupmembersince timestamptz default CURRENT_TIMESTAMP, groupmember integer references public.user(userid) not null, groupmemberof integer references group_chat(groupchatid) not null)"

createGroupMessageTable :: IO ()
createGroupMessageTable = void . withDatabase $
        \conn -> execute_ conn "create table if not exists group_message (groupmessageid serial primary key, groupmessagesent timestamptz default CURRENT_TIMESTAMP, groupmessagesource integer references public.user(userid) not null, groupmessagedestination integer references group_chat(groupchatid) not null, groupmessage varchar(1024) not null)"  

createGroupAdminTable :: IO ()
createGroupAdminTable = void . withDatabase $
        \conn -> execute_ conn "create table if not exists group_admin (groupadminid serial primary key, groupadminsince timestamptz default CURRENT_TIMESTAMP, groupadmin integer references public.user(userid) not null, groupadminof integer references public.group_chat(groupchatid)"

