module Networking.Authentication where

import Control.Monad.HT (lift2)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Map as Map
import Networking.Session
import Networking.Response
import Util.User (authenticate, addUser)
import Database.Types
import Util.Password (createSalt, hashWithSalt)
import qualified Data.ByteString.Lazy as BSL
import Networking.Statuscodes

handleAuthentication :: SessionState -> T.Text -> T.Text -> IO Response
handleAuthentication sessionState username password = do
    mUser <- authenticate username password
    case mUser of
        Nothing -> return $ emptyResponse InvalidCredentials
        Just user -> do
            setUser sessionState user
            return okResponse

handleSignUp :: SessionState -> T.Text -> T.Text -> IO Response
handleSignUp sessionState username password = do
    mUser <- addUser username password
    case mUser of
        Nothing -> do
            return $ emptyResponse UsernameAlreadyTaken
        Just user -> do
            setUser sessionState user
            return okResponse

