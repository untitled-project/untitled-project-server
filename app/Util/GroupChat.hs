{-# LANGUAGE OverloadedStrings #-}
module Util.GroupChat where

import Database.Types
import Database.Database
import qualified Data.Text as T
import Database.PostgreSQL.Simple
import Data.Time
import Data.Maybe (listToMaybe, isJust)

getGroupchatByName :: T.Text -> IO (Maybe Groupchat)
getGroupchatByName name = listToMaybe <$> (withDatabase $ \conn -> query conn "select * from group_chat where groupchatname = ?" $ Only name)

doesGroupchatExist :: T.Text -> IO Bool
doesGroupchatExist name = isJust <$> getGroupchatByName name

addGroupchat :: T.Text -> IO (Maybe Groupchat)
addGroupchat name = do
    exists <- doesGroupchatExist name
    if exists then
        return Nothing
    else do
        listToMaybe <$> (withDatabase $ \conn -> query conn "insert into group_chat (groupchatname) values (?) returning *" $ Only name)

addUserToGroupchat :: User -> Groupchat -> IO (Maybe GroupMember)
addUserToGroupchat user group = listToMaybe <$> (withDatabase $ \conn -> query conn "insert into group_member (groupmember, groupmemberof) values (?, ?) returning *" (userid user, groupchatid group))

isUserInGroupchat :: User -> Groupchat -> IO Bool
isUserInGroupchat user group = not . null <$> ((withDatabase $ \conn -> query conn "select * from group_member where groupmember = ? and groupmemberof = ?" (userid user, groupchatid group)) :: IO [GroupMember])

sinceWhenIsUserInGroupchat :: User -> Groupchat -> IO (Maybe UTCTime)
sinceWhenIsUserInGroupchat user group = do
    [Only time] <- withDatabase $ \conn -> query conn "select groupmembersince from group_member where groupmember = ? and groupmemberof = ?" (userid user, groupchatid group)
    return time

sendGroupMessage :: User -> Groupchat -> T.Text -> IO (Maybe GroupMessage)
sendGroupMessage user group message = listToMaybe <$> (withDatabase $ \conn -> query conn "insert into group_message (groupmessagesource, groupmessagedestination, groupmessage) values (?, ?, ?) returning *" (userid user, groupchatid group, message))

getGroupMessages :: Groupchat -> IO [GroupMessage]
getGroupMessages group = withDatabase $ \conn -> query conn "select * from  group_message where groupmessagedestination = ?" (Only $ groupchatid group)

getGroupMessagesSince :: UTCTime -> Groupchat -> IO [GroupMessage]
getGroupMessagesSince time group = withDatabase $ \conn -> query conn "select * from group_message where groupmessagedestination = ? and groupmessagesent < ?" (groupchatid group, time)

isGroupAdmin :: User -> Groupchat -> IO Bool
isGroupAdmin user group = isJust <$> getGroupAdmin user group

getGroupAdmin :: User -> Groupchat -> IO (Maybe GroupAdmin)
getGroupAdmin user group = listToMaybe <$> (withDatabase $ \conn -> query conn "select * from group_admin where groupadmin = ? and groupadminof = ?" (userid user, groupchatid group))
            

