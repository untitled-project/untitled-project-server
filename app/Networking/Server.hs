module Networking.Server
( Server
, mkServer
, run
) where

import Control.Monad (unless, forever, void)
import Control.Monad.HT (lift2)
import Control.Concurrent (forkFinally)
import qualified Control.Exception as E
import Network.Socket
import Network.Socket.ByteString as SBS (recv, sendAll)
import Data.ByteString as BS (null)
import Networking.Request (readRequest)
import Networking.Response (encodeResponse, emptyResponse)
import Networking.Session
import Networking.RequestHandling
import Networking.Statuscodes

data Server = Server
              { mainSock :: Socket
              , notifySock :: Socket
              } deriving (Eq, Show)


mkServer :: String -> String -> IO Server
mkServer port notifyPort = lift2 Server (socketOn port) (socketOn notifyPort)
    where
        socketOn port = resolve port >>= open
        resolve port = let hints = defaultHints { addrFlags = [AI_PASSIVE]
                                                , addrSocketType = Stream
                                                }
                       in head <$> getAddrInfo (Just hints) Nothing (Just port)
        open addr = do
            sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
            setSocketOption sock ReuseAddr 1
            setCloseOnExecIfNeeded $ fdSocket sock
            bind sock (addrAddress addr)
            listen sock 10
            return sock

run :: Server -> IO ()
run server = do
    forkFinally (runNotify $ notifySock server) (\_ -> close $ notifySock server)
    E.bracket (return $ mainSock server) close runMain

runNotify :: Socket -> IO ()
runNotify sock = do
    (conn, peer) <- accept sock
    -- add to list
    runNotify sock

runMain :: Socket -> IO ()
runMain sock = forever $ do
    (conn, peer) <- accept sock
    sessionState <- newSession conn 
    putStrLn $ "Connection from: " ++ show peer
    void $ forkFinally (handleClient sessionState) (\_ -> close conn)

handleClient :: SessionState -> IO ()
handleClient sessionState = do
    conn <- getConnection sessionState
    msg <- SBS.recv conn 1024
    unless (BS.null msg) $ do
        case readRequest msg of
            Nothing -> sendAll conn (encodeResponse $ emptyResponse MalformedRequest) >> handleClient sessionState
            Just request -> do
                 print request
                 result <- handleRequest sessionState request
                 print result
                 case result of
                    ResponseData response -> sendAll conn (encodeResponse response) >> handleClient sessionState
                    DropConnection -> return ()
