{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Networking.Response
( Response (..)
, ResponseBody(..)
, encodeResponse
, okResponse
, emptyResponse
, messageLikeToResponseMessage
) where

import Data.Word
import Data.Bits
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T
import GHC.Generics
import Data.Aeson
import qualified Database.Types as DB.Types
import qualified Util.User
import Data.Time
import Networking.Statuscodes

data Response = Response
                    { version :: Float
                    , status :: Statuscode
                    , body :: ResponseBody
                    } deriving (Eq, Show, Generic, ToJSON, FromJSON)

data ResponseBody = EmptyResponseBody
                  | DMResponseBody { directMessages :: [Message] }
                  | GroupMessagesResponseBody
                    { group :: T.Text
                    , groupMessages :: [Message]
                    }
                  deriving (Eq, Show, Generic, ToJSON, FromJSON)

data Message = Message
    { sender :: T.Text
    , message :: T.Text
    , timestamp :: UTCTime
    } deriving (Eq, Show, Generic, ToJSON, FromJSON)

messageLikeToResponseMessage :: DB.Types.MessageLike a => a -> IO (Maybe Message)
messageLikeToResponseMessage message = do
    mUser <- Util.User.getUserById (DB.Types.senderId message)
    case mUser of
        Nothing -> return Nothing
        Just user -> return . Just $ Message (DB.Types.username user) (DB.Types.message message) (DB.Types.messageSent message)

encodeResponse :: Response -> BS.ByteString
encodeResponse = BSL.toStrict . encode

okResponse :: Response
okResponse = Response 0.3 Ok EmptyResponseBody

emptyResponse :: Statuscode -> Response
emptyResponse status = Response 0.3 status EmptyResponseBody

