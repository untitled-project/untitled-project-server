module Util.Password (hashWithSalt, createSalt, createSaltAndHash) where

import Data.Digest.Pure.SHA
import System.Random
import Data.Word
import Control.Monad (replicateM)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Data.Text
import Data.Text.Encoding

-- |Appends a salt to a password and hashes the result with sha512.
hashWithSalt :: BSL.ByteString -> BSL.ByteString -> BSL.ByteString
hashWithSalt password salt = bytestringDigest . sha512 $ BSL.append password salt

-- |Creates a 64 byte ByteString salt from random values.
createSalt :: IO BS.ByteString
createSalt = BS.pack <$> replicateM 64 randomWord8

createSaltAndHash :: Text -> IO (BS.ByteString, BS.ByteString)
createSaltAndHash password = do
    salt <- createSalt
    return (BSL.toStrict $ hashWithSalt (BSL.fromStrict . encodeUtf8 $ password) (BSL.fromStrict salt), salt)

randomWord8 :: IO Word8
randomWord8 = fst . random <$> newStdGen
