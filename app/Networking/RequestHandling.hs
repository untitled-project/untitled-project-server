module Networking.RequestHandling
( ProcessingResult (..)
, handleRequest
) where

import qualified Data.Text as T
import Networking.Request hiding (username)
import Networking.Response
import Networking.Session
import Networking.Authentication
import Networking.DirectMessaging
import Networking.GroupMessaging
import Database.Types (username, User)
import Networking.Statuscodes

data ProcessingResult = ResponseData Response
                      | DropConnection
                        deriving (Eq, Show)

handleRequest :: SessionState -> Request -> IO ProcessingResult
handleRequest sessionState (Request version (AuthRequest username password)) = ResponseData <$> handleAuthentication sessionState username password
handleRequest sessionState (Request version (SignUpRequest username password)) = ResponseData <$> handleSignUp sessionState username password
handleRequest sessionState (Request version (SendDMRequest receiver msg)) = withUser sessionState $ \user -> handleSendDirectMessage user receiver msg
handleRequest sessionState (Request version (GetDMRequest time)) = withUser sessionState $ \user -> handleGetDirectMessages user time
handleRequest sessionState (Request version (GetAllDMRequest)) = withUser sessionState handleGetAllDirectMessages
handleRequest sessionState (Request version (CreateGroupRequest groupName)) = ResponseData <$> handleCreateGroup groupName
handleRequest sessionState (Request version (JoinGroupRequest groupName newMember)) = withUser sessionState $ \admin -> handleJoinGroup admin newMember groupName
handleRequest sessionState (Request version (GetAllGroupMessagesRequest groupName)) = withUser sessionState $ \user -> handleGetAllGroupMessages user groupName
handleRequest sessionState (Request version (AddGroupAdminRequest groupName newAdminName)) = withUser sessionState $ \admin -> handleAddGroupAdmin admin newAdminName groupName

withUser :: SessionState -> (User -> IO Response) -> IO ProcessingResult
withUser sessionState handler = do
    mUser <- getUser sessionState
    case mUser of
        Nothing   -> return . ResponseData $ emptyResponse AuthorizationRequired
        Just user -> ResponseData <$> handler user

