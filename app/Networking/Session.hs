module Networking.Session where

import Control.Concurrent
import Network.Socket
import Database.Types (User)

data Session = Session
                { mainConn :: Socket
                , notifyConn :: Maybe Socket
                , user :: Maybe User
                }

data SessionState = SessionState (MVar Session)


newSession :: Socket -> IO SessionState
newSession conn = do
    m <- newMVar $ Session conn Nothing Nothing
    return $ SessionState m


getConnection :: SessionState -> IO Socket
getConnection (SessionState m) = mainConn <$> readMVar m

withNotifyConn :: SessionState -> (Maybe Socket -> IO a) -> IO a
withNotifyConn (SessionState m) f = withMVar m $ \session -> f $ notifyConn session


setUser :: SessionState -> User -> IO ()
setUser (SessionState m) user = do
    session <- takeMVar m
    putMVar m $ Session (mainConn session) (notifyConn session) (Just user)

getUser :: SessionState -> IO (Maybe User)
getUser (SessionState m) = user <$> readMVar m
